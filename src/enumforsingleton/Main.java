package enumforsingleton;

public class Main {
    public static void main(String[] args) {
        EmailPort emailPort = EmailPort.INSTANCE;

        EmailPort emailPort1 = EmailPort.getInstance();

        System.out.println(emailPort);
        System.out.println(emailPort1);
    }
}
