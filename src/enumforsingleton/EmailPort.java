package enumforsingleton;

public enum EmailPort {

    //always uses an constant value
    INSTANCE(32145);

    private int port;

    EmailPort(int port) {
        this.port = port;
    }

    public static EmailPort getInstance() {
        return INSTANCE;
    }

    @Override
    public String toString() {
        return "EmailPort{" +
                "port=" + port +
                '}';
    }
}


